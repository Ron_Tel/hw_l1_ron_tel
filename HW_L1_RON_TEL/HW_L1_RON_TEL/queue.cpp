#include <iostream>
#include "queue.h"

#define MAX_SIZE 10

using namespace std;
/*
func restarting queue with null's
*/
void initQueue(queue* q, unsigned int size) 
{
	q->_arr = new int [size];
	q->_maxSize = size;
	q->_currSize = 0;
	for (int i = 0 ; i < size ; i++) 
	{
		q->_arr[i] = NULL;
	}

}
/*
func deleting queue
*/
void cleanQueue(queue* q) 
{
	delete (q->_arr);
	delete (q);
}
/*
func adding new item to queue , perform in o(1) Complication level (bounos)
*/
void enqueue(queue* q, unsigned int newValue) 
{
	if (q->_currSize == q->_maxSize)
		return;
	
	memmove(q->_arr + 1, q->_arr, q->_currSize * sizeof(int));
	q->_arr[0] = newValue;
	q->_currSize = q->_currSize + 1;
}
/*
func removing item from queue , returning the item.
*/
int dequeue(queue* q) 
{
	int ret = q->_arr[q->_currSize - 1];
	if (q->_currSize == 0) 
	{
		return -1;
	}
	q->_arr[q->_currSize - 1] = NULL;
	q->_currSize = q->_currSize - 1;
	return ret;
}

