#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct Node 
{
	unsigned int val;
	struct Node* next;
}Node;

Node* addToList(Node* head , int val);
Node* removeFromList(Node* head);
void recursiveClean(Node* head , Node* placeHolder);
#endif // !LINKEDLIST_H
